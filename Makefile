VERSION := 0.2
PREFIX  := /usr/local
CFLAGS  += -Wall -Wextra -std=c99 -DVERSION='"$(VERSION)"'
LDLIBS  += -lpthread -lbsd

socks5server: socks5server.o

test: socks5server
	./test_proxy.py

analyze: clean
	scan-build make

install: all
	install -m 0755 -D -t $(PREFIX)/bin socks5server
	[ -f $(PREFIX)/etc/socks5server.conf ] || install -m 0644 -D -t $(PREFIX)/etc socks5server.conf
	install -m 0644 -D -t $(PREFIX)/lib/systemd/system socks5server.service
	sed -i 's|PREFIX|$(PREFIX)|g' $(PREFIX)/lib/systemd/system/socks5server.service

clean:
	rm -f socks5server *.o

.PHONY: test analyze install clean
