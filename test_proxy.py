#!/usr/bin/env python3

import socket
import struct
import subprocess
import time
import threading
import unittest


def get_free_port():
    s = socket.socket()
    s.bind(("", 0))
    return s.getsockname()[1]

SOCKS_PORT = get_free_port()
ECHO_PORT = get_free_port()
ECHO_PORT_B = struct.pack(">H", ECHO_PORT)


class SocketHelper(object):
    def init_socket(self):
        self.sock = socket.socket(socket.AF_INET)
        self.sock.connect(("localhost", SOCKS_PORT))

    def init_ipv6_socket(self):
        self.sock = socket.socket(socket.AF_INET6)
        self.sock.connect(("localhost", SOCKS_PORT))

    def destroy_socket(self):
        self.sock.close()

    def send(self, msg):
        l = self.sock.send(msg)
        self.assertEqual(len(msg), l)

    def send_proxy_length(self, x):
        self.send(("%s" % x).zfill(10).encode("ascii") + b"\n")

    def recv(self, expected_length):
        result = self.sock.recv(16384)
        self.assertEqual(expected_length, len(result), str(result))
        return result

    def assertEnd(self):
        try:
            result = self.sock.recv(1)
            self.assertEqual(0, len(result), str(result))
        except ConnectionResetError:
            return

    def assertAuthSuccess(self):
        result = self.recv(2)
        self.assertEqual(b"\x05\x00", result)

    def assertAuthFail(self):
        result = self.recv(2)
        self.assertEqual(b"\x05\xff", result)

    def assertRequestSuccess(self):
        self.assertRequestResponse(0)

    def assertRequestFail(self, reply):
        self.assertRequestResponse(reply)
        self.assertEnd()

    def assertRequestResponse(self, reply):
        reply = struct.pack(">B", reply)
        expected = b"\x05" + reply + b"\x00\x01\x00\x00\x00\x00\x00\x00"
        result = self.recv(10)
        self.assertEqual(expected, result)

class TestAuthNegotiation(SocketHelper, unittest.TestCase):
    def run(self, result=None):
        with SocksServer():
            unittest.TestCase.run(self, result)

    def setUp(self):
        self.init_socket()

    def tearDown(self):
        self.destroy_socket()

    def test_one_method_success(self):
        self.send(b"\x05\x01\x00")
        self.assertAuthSuccess()

    def test_two_methods_success_first(self):
        self.send(b"\x05\x02\x00\x80")
        self.assertAuthSuccess()

    def test_two_methods_success_second(self):
        self.send(b"\x05\x02\x80\x00")
        self.assertAuthSuccess()

    def test_no_methods_fail(self):
        self.send(b"\x05\x00")
        self.assertEnd()

    def test_no_matching_methods_fail(self):
        self.send(b"\x05\x01\x80")
        self.assertAuthFail()

    def test_invalid_version_fail(self):
        self.send(b"\x04\x01\x00")
        self.assertEnd()


class TestRequestNegotiation(SocketHelper, unittest.TestCase):
    def run(self, result=None):
        with SocksServer():
            unittest.TestCase.run(self, result)

    def setUp(self):
        self.init_socket()
        self.send(b"\x05\x01\x00")
        self.assertAuthSuccess()

    def tearDown(self):
        self.destroy_socket()

    def test_invalid_version(self):
        self.send(b"\x04\x01\x00\x01\x7f\x00\x00\x01\x00\01")
        self.assertRequestFail(1)

    def test_invalid_command(self):
        self.send(b"\x05\x02\x00\x01\x7f\x00\x00\x01\x00\x01")
        self.assertRequestFail(7)

    def test_invalid_reserved_section(self):
        self.send(b"\x05\x01\x01\x01\x7f\x00\x00\x01\x00\x01")
        self.assertRequestFail(1)

    def test_invalid_address_type(self):
        self.send(b"\x05\x01\x00\x09\x7f\x00\x00\x01\x00\x01")
        self.assertRequestFail(8)

    def test_ipv4_success(self):
        self.send(b"\x05\x01\x00\x01\x7f\x00\x00\x01" + ECHO_PORT_B)
        self.assertRequestSuccess()

    def test_ipv4_bad_port(self):
        self.send(b"\x05\x01\x00\x01\x7f\x00\x00\x01\xff\xff")
        self.assertRequestFail(4)

    def test_ipv4_bad_host(self):
        self.send(b"\x05\x01\x00\x01\x7f\x00\x00\x00\xff\xff")
        self.assertRequestFail(4)

    def test_dns_success(self):
        self.send(b"\x05\x01\x00\x03\x09localhost" + ECHO_PORT_B)
        self.assertRequestSuccess()

    def test_dns_remote_success(self):
        self.send(b"\x05\x01\x00\x03\x0bexample.com\x00P")
        self.assertRequestSuccess()

    def test_dns_bad_port(self):
        self.send(b"\x05\x01\x00\x03\x09localhost\xff\xff")
        self.assertRequestFail(4)

    def test_dns_bad_host(self):
        self.send(b"\x05\x01\x00\x03\x09f.invalid" + ECHO_PORT_B)
        self.assertRequestFail(4)

    def test_dns_invalid_host(self):
        self.send(b"\x05\x01\x00\x03\x00" + ECHO_PORT_B)
        self.assertEnd()

    def test_ipv6_success(self):
        self.send(b"\x05\x01\x00\x04" + (b"\x00"*15) + b"\x01" + ECHO_PORT_B)
        self.assertRequestSuccess()

    def test_ipv6_bad_port(self):
        self.send(b"\x05\x01\x00\x04" + (b"\x00"*15) + b"\x01" + b"\xff\xff")
        self.assertRequestFail(4)

    def test_ipv6_bad_host(self):
        self.send(b"\x05\x01\x00\x04" + b"\xfe\x80" + (b"\x00"*13) + b"\x01" + ECHO_PORT_B)
        self.assertRequestFail(4)


class ProxyPacketHelper(object):
    def test_one_packet(self):
        self.send_proxy_length(3)
        self.send(b"foo")
        result = self.recv(3)
        self.assertEqual(b"foo", result)
        self.assertEnd()

    def test_no_received_data(self):
        self.send_proxy_length(0)
        self.send(b"foo")
        self.assertEnd()

    def test_two_packets(self):
        self.send_proxy_length(6)

        self.send(b"foo")
        result = self.recv(3)
        self.assertEqual(b"foo", result)

        self.send(b"bar")
        result = self.recv(3)
        self.assertEqual(b"bar", result)

        self.assertEnd()

    def test_large_packet(self):
        msg = b"1234" * 1024
        self.send_proxy_length(len(msg))
        self.send(msg)
        count = len(msg)
        while count > 0:
            part = self.sock.recv(4)
            self.assertEqual(b"1234", part)
            count -= 4
        self.assertEnd()


class TestIPv4Proxy(SocketHelper, ProxyPacketHelper, unittest.TestCase):
    def run(self, result=None):
        with SocksServer():
            unittest.TestCase.run(self, result)

    def setUp(self):
        self.init_socket()

        self.send(b"\x05\x01\x00")
        self.assertAuthSuccess()

        self.send(b"\x05\x01\x00\x01\x7f\x00\x00\x01" + ECHO_PORT_B)
        self.assertRequestSuccess()

    def tearDown(self):
        self.destroy_socket()

class TestDNSProxy(SocketHelper, ProxyPacketHelper, unittest.TestCase):
    def run(self, result=None):
        with SocksServer():
            unittest.TestCase.run(self, result)

    def setUp(self):
        self.init_socket()

        self.send(b"\x05\x01\x00")
        self.assertAuthSuccess()

        self.send(b"\x05\x01\x00\x03\x09localhost" + ECHO_PORT_B)
        self.assertRequestSuccess()

    def tearDown(self):
        self.destroy_socket()

class TestIPv6Proxy(SocketHelper, ProxyPacketHelper, unittest.TestCase):
    def run(self, result=None):
        with SocksServer():
            unittest.TestCase.run(self, result)

    def setUp(self):
        self.init_socket()

        self.send(b"\x05\x01\x00")
        self.assertAuthSuccess()

        self.send(b"\x05\x01\x00\x04" + (b"\x00"*15) + b"\x01" + ECHO_PORT_B)
        self.assertRequestSuccess()

    def tearDown(self):
        self.destroy_socket()

class TestPermissions(SocketHelper, unittest.TestCase):
    def assert_connection_allowed(self):
        try:
            self.init_socket()
            self.send(b"\x05\x01\x00")
            self.assertAuthSuccess()
        finally:
            self.destroy_socket()

    def assert_ipv6_connection_allowed(self):
        try:
            self.init_ipv6_socket()
            self.send(b"\x05\x01\x00")
            self.assertAuthSuccess()
        finally:
            self.destroy_socket()

    def assert_connection_blocked(self):
        try:
            self.init_socket()
            self.send(b"\x05\x01\x00")
            self.assertEnd()
        except ConnectionResetError:
            pass
        finally:
            self.destroy_socket()

    def test_allow_all_connections(self):
        with SocksServer({"ALLOW_ALL": "1"}):
            self.assert_connection_allowed()

    def test_block_all_connections(self):
        with SocksServer({}):
            self.assert_connection_blocked()

    def test_allow_ipv4_host(self):
        with SocksServer({"ALLOW_HOST1": "127.0.0.1"}):
            self.assert_connection_allowed()

    def test_allow_ipv6_host(self):
        with SocksServer({"ALLOW_HOST1": "::1"}):
            self.assert_ipv6_connection_allowed()

    def test_allow_multiple_hosts(self):
        with SocksServer({"ALLOW_HOST1": "foo.invalid", "ALLOW_HOST2": "localhost"}):
            self.assert_connection_allowed()


class EchoServer(object):
    def __enter__(self):
        self.run = True

        self.sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(("", ECHO_PORT))
        self.sock.listen(5)

        self.thread = threading.Thread(target=self.run_echo_server)
        self.thread.start()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.run = False
        self.sock.shutdown(socket.SHUT_RDWR)
        self.thread.join()

    def run_echo_server(self):
        try:
            while self.run:
                client, addr = self.sock.accept()
                try:
                    self.handle_echo_client(client)
                finally:
                    client.close()
        except:
            if not self.run:
                return

    def handle_echo_client(self, client):
        line = client.recv(10+1)
        if not line:
            return
        num_bytes = int(line)

        while num_bytes > 0:
            # force the test app to handle many packets by using small ones
            data = client.recv(16)
            if not data:
                break
            num_bytes -= len(data)
            while data:
                l = client.send(data)
                data = data[l:]

class SocksServer(object):
    def __init__(self, extra_env={"ALLOW_ALL": "1"}):
        self.env = {}
        self.env["LISTEN_PORT"] = str(SOCKS_PORT)
        self.env.update(extra_env)
        self.log_output = open("out.log", "a")

    def __enter__(self):
        self.process = subprocess.Popen(
            args=["./socks5server"],
            stdout=self.log_output,
            stderr=self.log_output,
            env=self.env,
        )

        self.wait_for_port()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.log_output.close()
        self.process.terminate()
        self.process.wait()

    def wait_for_port(self):
        start_time = time.time()
        with socket.socket(socket.AF_INET) as s:
            while start_time + 10 > time.time():
                try:
                    s.connect(("localhost", SOCKS_PORT))
                    return
                except ConnectionRefusedError:
                    time.sleep(0.01)


if __name__ == "__main__":
    with EchoServer():
        unittest.main()
