# socks5server #

socks5server is a minimalist SOCKS5 proxy server.

## Requirements

* C99 compiler
* libbsd
* pthread

## Instructions

### Compile

    apt-get install build-essential libbsd
    make
    sudo make install

### Configuration

* Edit /usr/local/etc/socks5server.conf
* You must specify `LISTEN_PORT`
* To make an open proxy you can use `ALLOW_ALL=1`
* Alternatively you can have an unlimited number of `ALLOW_HOST_<anything>=<host>`
* If you choose to specify a list of allowed hosts you can use DNS names or IP addresses

## Bug reports

Please raise issues on the [Bitbucket project](https://bitbucket.org/delx/proxy/issues?status=new&status=open).
